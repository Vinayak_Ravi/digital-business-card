package com.example.digitalcard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digitalcard.R;
import com.example.digitalcard.pojo.ProductListModel;

import java.util.List;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyViewHolder> {

    private List<ProductListModel> productListModels;


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img_view;


        public MyViewHolder(View view) {
            super(view);

//            img_view = view.findViewById(R.id.image_view);

        }

    }

    Context context;

    public ProductListAdapter(List<ProductListModel> productListModels, Context mContext) {
        this.productListModels = productListModels;
        this.context = mContext;

    }


    @Override
    public ProductListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.productlist_item, parent, false);

        return new ProductListAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


//        ProductListModel t = productListModels.get(position);



    }

    @Override
    public int getItemCount() {
//        return productListModels.size();
        return 10;
    }
}

