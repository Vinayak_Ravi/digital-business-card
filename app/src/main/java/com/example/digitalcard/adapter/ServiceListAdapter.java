package com.example.digitalcard.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digitalcard.R;
import com.example.digitalcard.pojo.AllServiceListModel;

import java.util.List;

public class ServiceListAdapter extends RecyclerView.Adapter<ServiceListAdapter.MyViewHolder> {

    private List<AllServiceListModel> serviceListModels;


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView img_view;


        public MyViewHolder(View view) {
            super(view);

//            img_view = view.findViewById(R.id.image_view);

        }

    }

    Context context;

    public ServiceListAdapter(List<AllServiceListModel> serviceListModels, Context mContext) {
        this.serviceListModels = serviceListModels;
        this.context = mContext;

    }


    @Override
    public ServiceListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.productlist_item, parent, false);

        return new ServiceListAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


//        ServiceListModel t = ServiceListModels.get(position);


    }

    @Override
    public int getItemCount() {
//        return serviceListModels.size();
        return 10;
    }
}

