package com.example.digitalcard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.digitalcard.R;
import com.example.digitalcard.pojo.MultipePhotosModel;

import java.util.List;

public class MultiplePhotosAdapter extends RecyclerView.Adapter<MultiplePhotosAdapter.ViewHolder> {

    private List<MultipePhotosModel> multipePhotosModels;
    private MultiplePhotosAdapter.OnItemClickListener listener;

    private Context context;


    public MultiplePhotosAdapter(List<MultipePhotosModel> multipePhotosModels, Context ctx, MultiplePhotosAdapter.OnItemClickListener listener) {
        this.multipePhotosModels = multipePhotosModels;
        this.context = ctx;
        this.listener = listener;

    }

    public interface OnItemClickListener {
        void onItemClick(int postion);
    }

    @Override
    public MultiplePhotosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                          int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.photos_lay, parent, false);

        MultiplePhotosAdapter.ViewHolder viewHolder = new MultiplePhotosAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MultiplePhotosAdapter.ViewHolder holder,
                                 int position) {

        MultipePhotosModel multipePhotosModel = multipePhotosModels.get(position);
        Glide.with(context).load(multipePhotosModel.getImage_url()).into(holder.image);

    }

    @Override
    public int getItemCount() {
        return multipePhotosModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.image);

        }
    }
}