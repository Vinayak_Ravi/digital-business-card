package com.example.digitalcard.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digitalcard.R;
import com.example.digitalcard.pojo.AllProductListModel;
import com.example.digitalcard.pojo.ProductListModel;

import java.util.List;

public class AllProductListAdapter extends RecyclerView.Adapter<AllProductListAdapter.MyViewHolder> {

    private List<AllProductListModel> allProductListModels;
    private AllProductListAdapter.OnItemClickListener listener;


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView productName, edit;

        public MyViewHolder(View view) {
            super(view);

            productName = view.findViewById(R.id.productName);
            edit = view.findViewById(R.id.edit);

        }

    }

    Context context;

    public interface OnItemClickListener {
        void onItemClick(int postion, String id,String pro_name,String pro_currency,String pro_price,String images);
    }

    public AllProductListAdapter(List<AllProductListModel> allProductListModels, Context mContext, AllProductListAdapter.OnItemClickListener listener) {
        this.allProductListModels = allProductListModels;
        this.context = mContext;
        this.listener = listener;

    }


    @Override
    public AllProductListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.allproductlist_item, parent, false);

        return new AllProductListAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        AllProductListModel t = allProductListModels.get(position);
        holder.productName.setText(t.getProductName());
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position, t.get_id(),t.getProductName(),t.getProduuctCurrencytype(),t.getProductPrice(),t.getImages());
            }
        });


    }

    @Override
    public int getItemCount() {
        return allProductListModels.size();
    }
}

