package com.example.digitalcard.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.digitalcard.R;
import com.example.digitalcard.pojo.AllServiceListModel;

import java.util.List;

public class AllServiceListAdapter extends RecyclerView.Adapter<AllServiceListAdapter.MyViewHolder> {

    private List<AllServiceListModel> allServiceListModels;
    private AllServiceListAdapter.OnItemClickListener listener;


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView serviceName, serviceedit;

        public MyViewHolder(View view) {
            super(view);
            serviceName = view.findViewById(R.id.serviceName);
            serviceedit = view.findViewById(R.id.serviceedit);

        }

    }

    Context context;

    public AllServiceListAdapter(List<AllServiceListModel> AllServiceListModels, Context mContext, AllServiceListAdapter.OnItemClickListener listener) {
        this.allServiceListModels = AllServiceListModels;
        this.context = mContext;
        this.listener = listener;

    }

    public interface OnItemClickListener {
        void onItemClick(int postion, String id, String pro_name, String pro_currency, String pro_price, String images);
    }


    @Override
    public AllServiceListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context)
                .inflate(R.layout.allservicelist_item, parent, false);

        return new AllServiceListAdapter.MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {


        AllServiceListModel serviceListModel = allServiceListModels.get(position);
        holder.serviceName.setText(serviceListModel.getProductName());
        holder.serviceedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position, serviceListModel.get_id(), serviceListModel.getProductName(), serviceListModel.getProduuctCurrencytype(), serviceListModel.getProductPrice(), serviceListModel.getImages());
            }
        });


    }

    @Override
    public int getItemCount() {
        return allServiceListModels.size();
    }

}

