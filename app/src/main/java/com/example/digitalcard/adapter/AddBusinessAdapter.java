package com.example.digitalcard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.recyclerview.widget.RecyclerView;

import com.example.digitalcard.R;
import com.example.digitalcard.pojo.AddBusinessModel;

import java.util.List;


public class AddBusinessAdapter extends RecyclerView.Adapter<AddBusinessAdapter.ViewHolder> {

    private List<AddBusinessModel> addBusinessModels;
    private Context context;

    private int lastSelectedPosition = -1;

    public AddBusinessAdapter(List<AddBusinessModel> addBusinessModels, Context ctx) {
        this.addBusinessModels = addBusinessModels;
        this.context = ctx;

    }

    @Override
    public AddBusinessAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                            int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.addbusiness_item, parent, false);

        AddBusinessAdapter.ViewHolder viewHolder = new AddBusinessAdapter.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AddBusinessAdapter.ViewHolder holder,
                                 int position) {

        //since only one radio button is allowed to be selected,
        // this condition un-checks previous selections
//        holder.primaryAddress.setChecked(lastSelectedPosition == position);
    }

    @Override
    public int getItemCount() {
//        return addressListModel.size();
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        public RadioButton primaryAddress;

        public ViewHolder(View view) {
            super(view);

//            primaryAddress = (RadioButton) view.findViewById(R.id.primaryAddress);
//
//            primaryAddress.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    lastSelectedPosition = getAdapterPosition();
//                    notifyDataSetChanged();
//
////                    Toast.makeText(AddressListAdapter.this.context,
////                            "selected offer is " + offerName.getText(),
////                            Toast.LENGTH_LONG).show();
//                }
//            });
        }
    }
}