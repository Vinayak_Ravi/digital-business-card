package com.example.digitalcard.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.amazonaws.mobile.client.AWSMobileClient;
import com.example.digitalcard.R;
import com.example.digitalcard.fragments.AddBusiness;
import com.example.digitalcard.fragments.BusinessDetail;
import com.example.digitalcard.fragments.HomeFragment;
import com.example.digitalcard.fragments.ProfileFragment;
import com.example.digitalcard.fragments.ThemeFragment;
import com.example.digitalcard.utils.Api;
import com.example.digitalcard.utils.Functions;
import com.example.digitalcard.utils.NetworkClient;
import com.example.digitalcard.utils.Variables;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayDeque;
import java.util.Deque;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.digitalcard.utils.ApiUrls.about_Us;
import static com.example.digitalcard.utils.ApiUrls.cardData;

/**
 * Created by Vinayak Ravi 20/04/2021
 */
public class HomePage extends AppCompatActivity /*implements BottomNavigationView.OnNavigationItemSelectedListener*/ {

    BottomNavigationView bottom_navigation;
    BottomSheetDialog bottomSheetDialog;
    Deque<Integer> integerDeque = new ArrayDeque<>(3);
    boolean flag = true;
    Toolbar toolbar;
    ImageView add_business, add_home;
    private int mSelectedItem;
    private static final String SELECTED_ITEM = "arg_selected_item";
    public static SharedPreferences sharedPreferences;
    public static String user_id;
    public static String f_name;
    public static String l_name;
    public static String e_mail;
    public static String gender;
    public static String mobile;
    public static String login;
    public static String businessCreate;
    public static String business_ID;
    public static String business_Name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#3ABEF0"));
        }
        initView();
        SharedPreferences prefs = getSharedPreferences("login", MODE_PRIVATE);
        user_id = prefs.getString(Variables.token, "");
        f_name = prefs.getString(Variables.firstName, "");
        l_name = prefs.getString(Variables.lastName, "");
        e_mail = prefs.getString(Variables.email, "");
        mobile = prefs.getString(Variables.phoneNo, "");
        gender = prefs.getString(Variables.gender, "");
        login = prefs.getString(Variables.isLogin, "");
        businessCreate = prefs.getString(Variables.createBusiness, "");
        business_ID = prefs.getString(Variables.businessId, "");
        business_Name = prefs.getString(Variables.businessName, "");
        Log.e("Token", user_id);
        Log.e("business_ID", business_ID);

        if (businessCreate != null && businessCreate.equals("1")) {
            Intent i = new Intent(HomePage.this, BusinessName.class);
            startActivity(i);
        } else {
        }

        business_cardDatas(this);

        setSupportActionBar(toolbar);

//        loadFragments(new HomeFragment());
//        bottom_navigation.setOnNavigationItemSelectedListener(this);

        bottom_navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectFragment(item);
                return true;
            }
        });

        MenuItem selectedItem;
        if (savedInstanceState != null) {
            mSelectedItem = savedInstanceState.getInt(SELECTED_ITEM, 0);
            selectedItem = bottom_navigation.getMenu().findItem(mSelectedItem);
        } else {
            selectedItem = bottom_navigation.getMenu().getItem(0);
        }

        selectFragment(selectedItem);

        add_business.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomePage.this, BusinessName.class);
                startActivity(i);
            }
        });


//        integerDeque.push(R.id.home);

//        loadFragment(new HomeFragment());
//        bottom_navigation.setSelectedItemId(R.id.home);

//        bottom_navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
//            @Override
//            public boolean onNavigationItemSelected(MenuItem menuItem) {
//                int id = menuItem.getItemId();
//
//                if (integerDeque.contains(id)) {
//                    if (id == R.id.home) {
//                        if (integerDeque.size() != 1) {
//                            if (flag) {
//                                integerDeque.addFirst(R.id.home);
//                                flag = false;
//                            }
//                        }
//                    }
//                    integerDeque.remove(id);
//                }
//
//                integerDeque.push(id);
//                loadFragment(getFragment(menuItem.getItemId()));
//                return true;
//            }
//        });


    }

    public static void business_cardDatas(Activity activity) {
        Api apiInterface = NetworkClient.getRetrofitClient(activity).create(Api.class);
        String url = cardData;
        Log.e("cardData", url);

        Call<okhttp3.ResponseBody> callqr = apiInterface.businessCardData(url);
        callqr.enqueue(new Callback<okhttp3.ResponseBody>() {
            @Override
            public void onResponse(Call<okhttp3.ResponseBody> call, Response<okhttp3.ResponseBody> response) {
                try {
                    Functions.cancel_loader();
                    JSONObject main = new JSONObject(response.body().string());
                    Log.e("response##", String.valueOf(main));
                    JSONObject data = main.getJSONObject("data");
                    Log.e("data", data.toString());
                    JSONObject aboutUs = data.getJSONObject("aboutUs");
                    Variables.AboutusObject = String.valueOf(data.getJSONObject("aboutUs"));
                    JSONArray mediaData = data.getJSONArray("socialMediaLink");
                    Variables.SocialMediaDatas = String.valueOf(mediaData);
                    Log.e("aboout", aboutUs.toString());
                    JSONArray product_List = data.getJSONArray("products");
                    Variables.productList = String.valueOf(product_List);
                    JSONArray service_List = data.getJSONArray("services");
                    Variables.serviceList = String.valueOf(service_List);


                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("exe^^^", e.toString());
                }
            }

            @Override
            public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {

            }
        });
    }


    private void selectFragment(MenuItem item) {
        Fragment frag = null;
        item.setCheckable(true);
        // init corresponding fragment
        switch (item.getItemId()) {
            case R.id.home:

                HomeFragment fragment = new HomeFragment();
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.frame_container, fragment);
                ft.commit();


                break;
            case R.id.bussiness:


                BusinessDetail fragmentone = new BusinessDetail();
                FragmentTransaction ft_one = getSupportFragmentManager().beginTransaction();
                ft_one.replace(R.id.frame_container, fragmentone);
                ft_one.commit();

                break;
            case R.id.add_business:

                add_business.setVisibility(View.VISIBLE);
                AddBusiness tabFragmentTwo = new AddBusiness();
                FragmentTransaction ft_two = getSupportFragmentManager().beginTransaction();
                ft_two.replace(R.id.frame_container, tabFragmentTwo);
                ft_two.commit();


                break;
            case R.id.theme:


                ThemeFragment tabFragmentthree = new ThemeFragment();
                FragmentTransaction ft_three = getSupportFragmentManager().beginTransaction();
                ft_three.replace(R.id.frame_container, tabFragmentthree);
                ft_three.commit();


                break;
            case R.id.profile:


                ProfileFragment tabFragmentfour = new ProfileFragment();
                FragmentTransaction ft_four = getSupportFragmentManager().beginTransaction();
                ft_four.replace(R.id.frame_container, tabFragmentfour);
                ft_four.commit();


                break;
        }

        // update selected item
        mSelectedItem = item.getItemId();


        if (frag != null) {
            HomeFragment fragment = new HomeFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.frame_container, fragment);
            ft.commit();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(SELECTED_ITEM, mSelectedItem);
        super.onSaveInstanceState(outState);
    }

//    private Fragment getFragment(int itemId) {
//        switch (itemId) {
//            case R.id.home:
//                bottom_navigation.getMenu().getItem(0).setChecked(true);
//                add_business.setVisibility(View.GONE);
//                return new HomeFragment();
//            case R.id.bussiness:
//                bottom_navigation.getMenu().getItem(1).setChecked(true);
//                add_business.setVisibility(View.GONE);
//                return new BusinessDetail();
//            case R.id.add_business:
//                bottom_navigation.getMenu().getItem(2).setChecked(true);
//                add_business.setVisibility(View.VISIBLE);
//                add_business.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent i = new Intent(HomePage.this, BusinessName.class);
//                        startActivity(i);
//                    }
//                });
//
//                return new AddBusiness();
//
//            case R.id.theme:
//                bottom_navigation.getMenu().getItem(3).setChecked(true);
//                add_business.setVisibility(View.GONE);
//                return new ThemeFragment();
//            case R.id.profile:
//                bottom_navigation.getMenu().getItem(4).setChecked(true);
//                add_business.setVisibility(View.GONE);
//                return new ProfileFragment();
//
//
//        }
//
//        bottom_navigation.getMenu().getItem(0).setChecked(true);
//        return new HomeFragment();


//    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar);
        add_business = findViewById(R.id.add_business);
        add_home = findViewById(R.id.add_home);
        bottom_navigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
    }


//    public void loadFragment(Fragment fragment) {
//        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.replace(R.id.frame_container, fragment, fragment.getClass().getSimpleName());
//        transaction.addToBackStack(null);
//        transaction.commit();
//    }
//
//    @Override
//    public void onBackPressed() {
//        integerDeque.pop();
//        if (!integerDeque.isEmpty()) {
//            loadFragment(getFragment(integerDeque.peek()));
//        } else {
//            finish();
//        }
//    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        if (getSupportFragmentManager().findFragmentByTag(getClass().getSimpleName()) != null)
//            getSupportFragmentManager().findFragmentByTag(getClass().getSimpleName()).setRetainInstance(true);
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (getSupportFragmentManager().findFragmentByTag(getClass().getSimpleName()) != null)
//            getSupportFragmentManager().findFragmentByTag(getClass().getSimpleName()).getRetainInstance();
//    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
//            Toast.makeText(HomePage.this, "TEXTTTTTTTTTTT", Toast.LENGTH_SHORT).show();
        }
    }


//    public boolean loadFragments(Fragment fragment) {
//        if (fragment != null) {
//            getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).addToBackStack(null).commit();
//        }
//
//        return true;
//    }
//
//    @Override
//    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//
//        Fragment fragment = null;
//        switch (item.getItemId()) {
//            case R.id.home:
//                add_business.setVisibility(View.GONE);
//                fragment = new HomeFragment();
//                break;
//            case R.id.bussiness:
//                add_business.setVisibility(View.GONE);
//                fragment = new BusinessDetail();
//                break;
//            case R.id.add_business:
//                add_business.setVisibility(View.VISIBLE);
//                fragment = new AddBusiness();
//                break;
//
//
//            case R.id.theme:
//                add_business.setVisibility(View.GONE);
//                fragment = new ThemeFragment();
//                break;
//            case R.id.profile:
//                add_business.setVisibility(View.GONE);
//                fragment = new ProfileFragment();
//                break;
//
//
//        }
//        return loadFragments(fragment);
//    }

    @Override
    public void onBackPressed() {
        MenuItem homeItem = bottom_navigation.getMenu().getItem(0);

        if (mSelectedItem != homeItem.getItemId()) {

            selectFragment(homeItem);

            // Select home item
            bottom_navigation.setSelectedItemId(homeItem.getItemId());
        } else {
            super.onBackPressed();
        }
    }
}