package com.example.digitalcard.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.digitalcard.R;
import com.example.digitalcard.pojo.CreateUserRequest;
import com.example.digitalcard.pojo.CreateUserResponse;
import com.example.digitalcard.pojo.Data;
import com.example.digitalcard.pojo.User;
import com.example.digitalcard.utils.Api;
import com.example.digitalcard.utils.ApiUrls;
import com.example.digitalcard.utils.Functions;
import com.example.digitalcard.utils.NetworkClient;
import com.example.digitalcard.utils.Variables;
import com.google.android.material.chip.Chip;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.digitalcard.utils.Functions.hideKeyboard;

/**
 * Created by Vinayak Ravi 20/04/2021
 */

public class RegistrationActivity extends AppCompatActivity {

    TextInputLayout mobileNumber, firstName, lastName, emailId;
    EditText mobile_number, first_name, last_name, email_id;
    RadioButton male, female, others;
    Animation fade_anim, slide_in_from_left, slide_in_from_left_two, slide_in_from_left_three, slide_in_from_left_four, slide_in_from_right, slide_in_from_bottom;
    TextView btnSignIn;
    ConstraintLayout registerLay;
    String phoneNumber;
    String countryCode;
    SharedPreferences sharedPreferences;
    ProgressBar progress_bar;
    String gender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        initView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#3ABEF0"));
        }
        sharedPreferences = getSharedPreferences(Variables.loginData, Context.MODE_PRIVATE);

        phoneNumber = getIntent().getStringExtra("PhoneNumber");
        countryCode = getIntent().getStringExtra("code");

        registerLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (male.isChecked()) {
                    gender = "Male";
                } else if (female.isChecked()) {
                    gender = "Female";
                } else if (others.isChecked()) {
                    gender = "Others";
                }
                Functions.Show_loader(RegistrationActivity.this, false, true);
                hideKeyboard(RegistrationActivity.this);
                if (!first_name.getText().toString().equals("") && !last_name.getText().toString().equals("") && !gender.equals("") && !email_id.getText().toString().equals("")) {
                    create_user();
                } else {
                    if (first_name.getText().toString().equals("")) {
                        first_name.setError("Enter First Name");
                        first_name.setFocusable(true);
                    }
                    if (last_name.getText().toString().equals("")) {
                        last_name.setError("Enter Last Name");
                        last_name.setFocusable(true);
                    }

                    if (gender.equals("")) {
                        Toast.makeText(RegistrationActivity.this, "Please select Gender", Toast.LENGTH_SHORT).show();
                    }
                    if (email_id.getText().toString().equals("")) {
                        email_id.setError("Enter Email");
                        email_id.setFocusable(true);
                    }
                }


            }
        });

    }


    private void initView() {

        first_name = findViewById(R.id.firstName);
        last_name = findViewById(R.id.lastName);
        email_id = findViewById(R.id.email);
        male = findViewById(R.id.male);
        female = findViewById(R.id.female);
        others = findViewById(R.id.others);
        registerLay = findViewById(R.id.registerLay);
        progress_bar = findViewById(R.id.progress_bar);
        fade_anim = AnimationUtils.loadAnimation(this, R.anim.fade_anim);
        slide_in_from_left = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_left);
        slide_in_from_left_two = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_left_two);
        slide_in_from_left_three = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_left_three);
        slide_in_from_left_four = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_left_four);
        slide_in_from_right = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_right);
        slide_in_from_bottom = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_bottom);

    }


    private void create_user() {

        String firstname = first_name.getText().toString().trim();
        String lastname = last_name.getText().toString().trim();
        String mail = email_id.getText().toString().trim();


        Api apiInterface = NetworkClient.getRetrofitClient(RegistrationActivity.this).create(Api.class);


        CreateUserRequest createUserRequest = new CreateUserRequest(firstname, lastname, mail, Long.parseLong(phoneNumber), Integer.parseInt(countryCode), gender);
        String json = new Gson().toJson(createUserRequest);
        Log.e("json******", json);
        Call<okhttp3.ResponseBody> callqr = apiInterface.createUser(createUserRequest);
        Log.e("callqr", String.valueOf(callqr));


        callqr.enqueue(new Callback<okhttp3.ResponseBody>() {
            @Override
            public void onResponse(Call<okhttp3.ResponseBody> call, retrofit2.Response<okhttp3.ResponseBody> response) {
                try {
                    Functions.cancel_loader();
                    Gson gson = new Gson();
                    String respo = gson.toJson(response.body());
                    Log.e("respo",respo);
                    JSONObject jsonObject1 = new JSONObject(response.body().string());
//                    JSONObject jsonObject1 = new JSONObject(response.errorBody().string());
                    Log.e("errror",jsonObject1.toString());
                    Log.e("response##", String.valueOf(jsonObject1));
                    JSONObject jsonObject2 = jsonObject1.getJSONObject("data");
                    String token = jsonObject2.getString("token");
                    Log.e("token", token);
                    JSONObject jsonObject3 = jsonObject2.getJSONObject("user");
                    String email = jsonObject3.getString("email");
                    String firstName = jsonObject3.getString("firstName");
                    String gender = jsonObject3.getString("gender");
                    String lastName = jsonObject3.getString("lastName");
                    String phoneNumber = jsonObject3.getString("phoneNumber");

                    SharedPreferences.Editor editor = getSharedPreferences("login", MODE_PRIVATE).edit();
                    editor.putString(Variables.token, token);
                    editor.putString(Variables.firstName, firstName);
                    editor.putString(Variables.lastName, lastName);
                    editor.putString(Variables.email, email);
                    editor.putString(Variables.phoneNo, phoneNumber);
                    editor.putString(Variables.gender, gender);
                    editor.putString(Variables.isLogin, "true");
                    editor.putString(Variables.createBusiness, "1");
                    editor.apply();

                    Intent i = new Intent(RegistrationActivity.this, HomePage.class);
                    startActivity(i);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("exe^^^", e.toString());
                }
            }

            @Override
            public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {
                Toast.makeText(RegistrationActivity.this, "Try again Later", Toast.LENGTH_SHORT).show();

            }
        });


    }

/*
    private void create_user() {


        String firstname = first_name.getText().toString().trim();
        String lastname = last_name.getText().toString().trim();
        String mail = email_id.getText().toString().trim();
        String gender = null;
        if (male.isChecked()) {
            gender = "Male";
        } else if (female.isChecked()) {
            gender = "Female";
        } else if (others.isChecked()) {
            gender = "Others";
        }

        Api apiInterface = NetworkClient.getRetrofitClient(RegistrationActivity.this).create(Api.class);


        CreateUserRequest createUserRequest = new CreateUserRequest(firstname, lastname, mail, Long.parseLong(phoneNumber), Integer.parseInt(countryCode), gender);
        String json = new Gson().toJson(createUserRequest);
        Log.e("json******", json);
        Call<CreateUserResponse> call = apiInterface.createUser(createUserRequest);
        call.enqueue(new Callback<CreateUserResponse>() {
            @Override
            public void onResponse(Call<CreateUserResponse> call, Response<CreateUserResponse> response) {
                Functions.cancel_loader();
                CreateUserResponse createUserResponse = response.body();
                String json = new Gson().toJson(createUserResponse);
                Log.e("json******", json);
                Data userResponseData = createUserResponse.getData();
                String json1 = new Gson().toJson(userResponseData);
                Log.e("json******", json1);
                String data1 = userResponseData.getToken();
                Log.e("token^^^^^^^^^^^", data1);
                User user = userResponseData.getUser();
                String mail = user.getEmail();
                Log.e("email^^^^^^^^^^^^^^^^", mail);


            }

            @Override
            public void onFailure(Call<CreateUserResponse> call, Throwable t) {
                Functions.cancel_loader();
            }
        });
    }
*/
}