package com.example.digitalcard.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.digitalcard.R;
import com.example.digitalcard.adapter.AddressListAdapter;
import com.example.digitalcard.adapter.AllServiceListAdapter;
import com.example.digitalcard.pojo.AddressListModel;
import com.example.digitalcard.pojo.AllServiceListModel;
import com.example.digitalcard.utils.Api;
import com.example.digitalcard.utils.Functions;
import com.example.digitalcard.utils.NetworkClient;
import com.example.digitalcard.utils.Variables;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.digitalcard.utils.ApiUrls.cardData;

/**
 * Created by Vinayak Ravi 20/04/2021
 */

public class AddAddress extends AppCompatActivity {
    FloatingActionButton add_address;
    BottomSheetDialog sheetDialog;
    RecyclerView addressList;
    AddressListAdapter addressListAdapter;
    private List<AddressListModel> addressListModels = new ArrayList<>();
    EditText address1, address2, currencyType, pincode, phoneNumber;
    ConstraintLayout sumbit_address;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#3ABEF0"));
        }
        initView();

        add_address.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetDialog = new BottomSheetDialog(AddAddress.this, R.style.BottomSheetTheme);

                View sheetView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.add_address_bottomsheet, (ViewGroup) findViewById(R.id.bottomSheet));
                address1 = sheetView.findViewById(R.id.address1);
                address2 = sheetView.findViewById(R.id.address2);
                pincode = sheetView.findViewById(R.id.pincode);
                currencyType = sheetView.findViewById(R.id.currencyType);
                phoneNumber = sheetView.findViewById(R.id.phoneNumber);
                sumbit_address = sheetView.findViewById(R.id.sumbit_address);
                sumbit_address.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                        if (!address1.getText().toString().equals("") && !address2.getText().toString().equals("") && !pincode.getText().toString().equals("") && !currencyType.getText().toString().equals("") && !phoneNumber.getText().toString().equals("")) {
                        pincodeData(pincode.getText().toString());
//                        }
                    }
                });

                sheetDialog.setContentView(sheetView);
                sheetDialog.setCanceledOnTouchOutside(true);
                sheetDialog.show();
            }
        });

        addressListAdapter = new AddressListAdapter(addressListModels, AddAddress.this);
        addressList.setLayoutManager(new LinearLayoutManager(AddAddress.this, LinearLayoutManager.VERTICAL, false));
        addressList.setNestedScrollingEnabled(false);
        addressList.setHasFixedSize(false);
        addressList.setAdapter(addressListAdapter);
        addressListAdapter.notifyDataSetChanged();


    }

    private void pincodeData(String pinCode) {
        String url = "http://www.postalpincode.in/api/pincode/" + pinCode;
        Api apiInterface = NetworkClient.getRetrofitClient(AddAddress.this).create(Api.class);


        Call<ResponseBody> callqr = apiInterface.pinCode(url);
        callqr.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<okhttp3.ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Functions.cancel_loader();
                    JSONObject main = new JSONObject(response.body().string());
                    Log.e("response##", String.valueOf(main));
                    String status = main.getString("Status");
                    if (status != null && status.equals("Success")) {
                        JSONArray arr = main.getJSONArray("PostOffice");
                        for (int i = 0; i < 1; i++) {
                            JSONObject object = arr.getJSONObject(i);
                            String city = object.getString("District");
                            String state = object.getString("State");
                            Log.e("city", city);
                            Log.e("state", state);

                            addAddress(city, state);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("exe^^^", e.toString());
                }
            }

            @Override
            public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {

            }
        });
    }

    private void addAddress(String city, String state) {
        String address_1 = address1.getText().toString();
        String address_2 = address2.getText().toString();
        String mobile = phoneNumber.getText().toString();
        String pin = pincode.getText().toString();
//        Api apiInterface = NetworkClient.getRetrofitClient(AddAddress.this).create(Api.class);
//
//
//        Call<ResponseBody> callqr = apiInterface.pinCode(url);
//        callqr.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<okhttp3.ResponseBody> call, Response<ResponseBody> response) {
//                try {
//                    Functions.cancel_loader();
//                    JSONObject main = new JSONObject(response.body().string());
//                    Log.e("response##", String.valueOf(main));
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    Log.e("exe^^^", e.toString());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {
//
//            }
//        });

    }

    private void initView() {
        add_address = findViewById(R.id.add_address);
        addressList = findViewById(R.id.addressList);
    }


}