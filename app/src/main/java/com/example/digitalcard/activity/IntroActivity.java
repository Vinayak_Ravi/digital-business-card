package com.example.digitalcard.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.digitalcard.R;
import com.example.digitalcard.adapter.IntroAdapter;
import com.google.android.material.tabs.TabLayout;

/**
 *Created by Vinayak Ravi 20/04/2021
 */

public class IntroActivity extends AppCompatActivity {

    TabLayout tabIndicator;
    int postion = 0;
    ImageView nextBtn;
    TextView preBtn, btnContinue;
    ViewPager viewPager;
    int bacground[] = {R.drawable.ic_group_top_design, R.drawable.ic_group_top_design, R.drawable.ic_group_top_design};
    int girlimage[] = {R.drawable.ic_intro_one, R.drawable.ic_intro_two, R.drawable.ic_intro_three};
    String heading[] = {"Create your card", "Save your card", "Share with friends and colleagues"};
    String description[] = {
            "Design your digistal \n Visisting card in two minutes",
            "Digital visiting card is accessible anytime \n  from anywhere",
            "Design your digital \n visiting card in 2 minutes"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#3ABEF0"));
        }

        tabIndicator = findViewById(R.id.tabIndicator);
        viewPager = findViewById(R.id.viewPager);
        preBtn = findViewById(R.id.preBtn);
        btnContinue = findViewById(R.id.btnContinue);
        IntroAdapter adapter = new IntroAdapter(IntroActivity.this, bacground, girlimage, heading, description);
        viewPager.setAdapter(adapter);
        tabIndicator.setupWithViewPager(viewPager);

        nextBtn = findViewById(R.id.nextBtn);

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                postion = viewPager.getCurrentItem();
                if (postion < bacground.length) {
                    postion++;
                    viewPager.setCurrentItem(postion);
                }
                if (postion == 3) {
                    intentTomainScreen();
                }
                if (postion == bacground.length - 1) {
//                    nextBtn.setText("Start");
                }


            }


            private void intentTomainScreen() {

                Intent MainIntent = new Intent(IntroActivity.this, HomePage.class);
                startActivity(MainIntent);
                finish();
            }
        });


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {

                if (position == 2) {
                    preBtn.setVisibility(View.GONE);
                    btnContinue.setVisibility(View.VISIBLE);
                    nextBtn.setVisibility(View.GONE);
//                    nextBtn.setText("Start");
                } else {
                    preBtn.setVisibility(View.VISIBLE);
                    btnContinue.setVisibility(View.GONE);
                    nextBtn.setVisibility(View.VISIBLE);
//                    nextBtn.setText("Next");
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        preBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                postion = viewPager.getCurrentItem();
                startActivity(new Intent(IntroActivity.this, LoginActivity.class));
                finish();
            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(IntroActivity.this, LoginActivity.class));
                finish();
            }
        });
    }
}