package com.example.digitalcard.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.digitalcard.R;

/**
 *Created by Vinayak Ravi 20/04/2021
 */

public class OtpActivity extends AppCompatActivity {

    EditText one, two, three, four;
    Animation slide_in_from_left,slide_in_from_right;
    Button btnContinue;
    Animation fade_anim;
    TextView resendOtp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        initView();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#3A2D78"));
        }


        String tmpHtml = "Didn't receive OTP? <font color='#FFA500'> <b>Resend OTP</b></font>";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            resendOtp.setText(Html.fromHtml(tmpHtml, Html.FROM_HTML_MODE_COMPACT));
        } else {
            resendOtp.setText(Html.fromHtml(tmpHtml));
        }


        four.startAnimation(slide_in_from_right);
        three.startAnimation(slide_in_from_right);
        two.startAnimation(slide_in_from_right);
        one.startAnimation(slide_in_from_right);

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OtpActivity.this, BusinessName.class);
                startActivity(i);
//                finish();
            }
        });

        btnContinue.startAnimation(fade_anim);
        resendOtp.startAnimation(slide_in_from_left);


    }

    private void initView() {
        one = findViewById(R.id.one);
        two = findViewById(R.id.two);
        three = findViewById(R.id.three);
        four = findViewById(R.id.four);
        resendOtp = findViewById(R.id.resendOtp);
        btnContinue = findViewById(R.id.btnContinue);
        fade_anim = AnimationUtils.loadAnimation(this, R.anim.fade_anim);
        slide_in_from_right = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_right);
        slide_in_from_left = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_left);
    }
}