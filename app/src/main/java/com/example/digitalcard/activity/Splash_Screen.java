package com.example.digitalcard.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.digitalcard.R;
import com.example.digitalcard.utils.Variables;

import in.codeshuffle.typewriterview.TypeWriterView;

/**
 * Created by Vinayak Ravi 20/04/2021
 */

public class Splash_Screen extends AppCompatActivity {

    ImageView logo;
    Animation splash_screen_anim;
    ConstraintLayout parent;
    TypeWriterView name;
    SharedPreferences sharedPreferences;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#3ABEF0"));
        }

        initView();


        name.setDelay(200);
        name.setWithMusic(true);
        name.animateText("Digital Business Card");
        SharedPreferences prefs = getSharedPreferences("login", MODE_PRIVATE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String login = prefs.getString(Variables.isLogin, "");
                if (login != null && login.equals("true")) {
                    Intent i = new Intent(Splash_Screen.this, HomePage.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(Splash_Screen.this, IntroActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        }, 4000);
    }

    private void initView() {
        logo = findViewById(R.id.logo);
        parent = findViewById(R.id.parent);
        name = findViewById(R.id.name);
        splash_screen_anim = AnimationUtils.loadAnimation(this, R.anim.splash_screen_anim);

    }

    @Override
    protected void onPause() {
        super.onPause();
        name.removeAnimation();

    }

    @Override
    protected void onStop() {
        super.onStop();
        name.removeAnimation();

    }
}