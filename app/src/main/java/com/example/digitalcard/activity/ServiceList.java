package com.example.digitalcard.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;
import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.example.digitalcard.R;
import com.example.digitalcard.adapter.AllProductListAdapter;
import com.example.digitalcard.adapter.AllServiceListAdapter;
import com.example.digitalcard.pojo.AddServicePojo;
import com.example.digitalcard.pojo.AllProductListModel;
import com.example.digitalcard.pojo.AllServiceListModel;
import com.example.digitalcard.pojo.Image;
import com.example.digitalcard.pojo.Name;
import com.example.digitalcard.utils.Api;
import com.example.digitalcard.utils.Functions;
import com.example.digitalcard.utils.NetworkClient;
import com.example.digitalcard.utils.Variables;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.digitalcard.activity.HomePage.business_ID;
import static com.example.digitalcard.utils.ApiUrls.addProducts;
import static com.example.digitalcard.utils.ApiUrls.addServices;
import static com.example.digitalcard.utils.ApiUrls.updateProducts;
import static com.example.digitalcard.utils.ApiUrls.updateServices;
import static com.example.digitalcard.utils.Variables.SUFFIX;
import static com.example.digitalcard.utils.Variables.bucketName;

/**
 * Created by Vinayak Ravi 20/04/2021
 */

public class ServiceList extends AppCompatActivity {

    FloatingActionButton add_service;
    BottomSheetDialog sheetDialog, updateSheetDialog;
    EditText serviceamount, currencyType, seviceName;
    RecyclerView serviceList;
    AllServiceListAdapter allServiceListAdapter;
    private List<AllServiceListModel> allServiceListModels = new ArrayList<>();
    ImageView close;
    ConstraintLayout sumbit;
    public List<Image> images = new ArrayList<>();
    CardView add_photos_card;
    public List<String> imageVideoPathList;
    int img_size = 0;
    LinearLayout addMultiplePhotos;
    public int imageVideo;
    String urlpath;
    JSONArray array = new JSONArray();
    String mainAddService;
    ConstraintLayout no_data;
    TextView sumbitService, addServiceText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_list);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#3ABEF0"));
        }

        initViews();

        mainAddService = getIntent().getStringExtra("add");
        if (mainAddService != null && !mainAddService.equals("")) {
            addService();
        }


        add_service.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addService();
            }
        });


        serviceListData();
    }

    //Add Service UI----------------------------------------------------------------------------------------------------
    private void addService() {
        images.clear();
        images = new ArrayList<>();
        array = new JSONArray();
        sheetDialog = new BottomSheetDialog(ServiceList.this, R.style.BottomSheetTheme);

        View sheetView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.add_service, (ViewGroup) findViewById(R.id.bottomSheet));
        serviceamount = sheetView.findViewById(R.id.serviceamount);
        seviceName = sheetView.findViewById(R.id.seviceName);
        currencyType = sheetView.findViewById(R.id.currencyType);
        addMultiplePhotos = sheetView.findViewById(R.id.addMultiplePhotos);
        add_photos_card = sheetView.findViewById(R.id.add_photos_card);
        sumbit = sheetView.findViewById(R.id.sumbit);
        serviceamount.setImeOptions(EditorInfo.IME_ACTION_DONE);
        close = sheetView.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sheetDialog.dismiss();
            }
        });

        sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!seviceName.getText().toString().equals("") && !currencyType.getText().toString().equals("") && !serviceamount.getText().toString().equals("")) {
                    for (int i = 0; i < array.length(); i++) {
                        try {
                            int j = array.length();
                            String url = String.valueOf(array.get(i));
                            int pos = i + 1;
                            Log.e("postiopn*******", String.valueOf(pos));
                            Log.e("leth*******", String.valueOf(array.length()));
                            fileUploadAdd(url, pos);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                }
            }
        });

        add_photos_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMultiple_Photos();
            }
        });

        sheetDialog.setContentView(sheetView);
        sheetDialog.setCanceledOnTouchOutside(true);
        sheetDialog.show();
    }

    private void addMultiple_Photos() {
        ImagePicker.create(ServiceList.this) // Activity or Fragment
                .includeVideo(true)
                .limit(10)
                .folderMode(true)
                .toolbarFolderTitle("Select Folder") // folder selection title
                .multi()
                .theme(R.style.PickerTheme) // must inherit ef_BaseTheme. please refer to sample
                .enableLog(false) // disabling log
                .start();
    }


    //Add Service Push------------------------------------------------------------------------------------------------------
    private void add_newService(String serviceName, String currencyType, String service_price) {
        Api apiInterface = NetworkClient.getRetrofitClient(ServiceList.this).create(Api.class);
        Name name = new Name(serviceName);
        AddServicePojo addServicePojo = new AddServicePojo(name, "", "", images, Integer.parseInt(service_price), currencyType, 1, true);
        String json = new Gson().toJson(addServicePojo);
        Log.e("JSON", json);
        String url = addServices + business_ID;
        Log.e("url", url);

        Call<ResponseBody> callqr = apiInterface.AddProducts(url, addServicePojo);
        Log.e("callqr", String.valueOf(callqr));


        callqr.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<okhttp3.ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Functions.cancel_loader();
                    JSONObject jsonObject1 = new JSONObject(response.body().string());
                    Log.e("response##", String.valueOf(jsonObject1));
                    sheetDialog.dismiss();
                    HomePage.business_cardDatas(ServiceList.this);
                    new Handler().postDelayed(
                            new Runnable() {
                                @Override
                                public void run() {
                                    serviceListData();
                                }
                            }, 3000);


                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("exe^^^", e.toString());
                }
            }

            @Override
            public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {
                Toast.makeText(ServiceList.this, "Try again Later", Toast.LENGTH_SHORT).show();

            }
        });


    }

    //Service List-------------------------------------------------------------------------------------------------------------
    private void serviceListData() {
        String data = Variables.serviceList;
        Log.e("data*************", data);
        try {
            allServiceListModels.clear();
            allServiceListModels = new ArrayList<>();
            if (data != null && !data.equals("[]")) {
                no_data.setVisibility(View.GONE);
                JSONArray jsonArray = new JSONArray(data);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = jsonArray.getJSONObject(i);
                    AllServiceListModel productListModel = new AllServiceListModel();
                    productListModel.set_id(object.getString("_id"));
                    JSONObject name = object.getJSONObject("name");
                    String productName = name.getString("en");
                    productListModel.setProductName(productName);
                    JSONArray array = object.getJSONArray("images");
                    productListModel.setImages(String.valueOf(array));
                    productListModel.setProductPrice(object.getString("price"));
                    productListModel.setProduuctCurrencytype(object.getString("currency"));
                    allServiceListModels.add(productListModel);


                }
            } else {
                no_data.setVisibility(View.VISIBLE);
            }
            allServiceListAdapter = new AllServiceListAdapter(allServiceListModels, this, new AllServiceListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int position, String id, String pro_name, String pro_currency, String pro_price, String images) {
                    int index = position;
                    Log.e("click^^^^^^^^", String.valueOf(index));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        updateServiceSheet(id, pro_name, pro_currency, pro_price, images);


                    }


                }
            });
            serviceList.setLayoutManager(new LinearLayoutManager(ServiceList.this, LinearLayoutManager.VERTICAL, false));
            serviceList.setAdapter(allServiceListAdapter);
            allServiceListAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("exe--------------", e.toString());
        }

    }

    //Update service UI-------------------------------------------------------------------------------------------------------------
    private void updateServiceSheet(String id, String pro_name, String pro_currency, String pro_price, String imagesObj) {
        images.clear();
        images = new ArrayList<>();
        array = new JSONArray();
        updateSheetDialog = new BottomSheetDialog(ServiceList.this, R.style.BottomSheetTheme);

        View updatesheetView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.add_service, (ViewGroup) findViewById(R.id.bottomSheet));

        add_photos_card = updatesheetView.findViewById(R.id.add_photos_card);
        addMultiplePhotos = updatesheetView.findViewById(R.id.addMultiplePhotos);
        addServiceText = updatesheetView.findViewById(R.id.addServiceText);
        sumbitService = updatesheetView.findViewById(R.id.sumbitService);
        close = updatesheetView.findViewById(R.id.close);
        sumbit = updatesheetView.findViewById(R.id.sumbit);
        seviceName = updatesheetView.findViewById(R.id.seviceName);
        serviceamount = updatesheetView.findViewById(R.id.serviceamount);
        currencyType = updatesheetView.findViewById(R.id.currencyType);
        sumbitService.setText("Update Service");
        addServiceText.setText("Update Service");
        serviceamount.setImeOptions(EditorInfo.IME_ACTION_DONE);

        try {
            JSONArray img = new JSONArray(imagesObj);
            for (int i = 0; i < img.length(); i++) {
                JSONObject object = img.getJSONObject(i);
                String image = object.getString("link");
                addImages(image, i);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateSheetDialog.dismiss();

            }
        });
        seviceName.setText(pro_name);
        serviceamount.setText(pro_price);
        currencyType.setText(pro_currency);

        sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Data", array.toString());
                for (int i = 0; i < array.length(); i++) {
                    try {
                        int j = array.length();
                        String url = String.valueOf(array.get(i));
                        int pos = i + 1;
                        Log.e("postiopn*******", String.valueOf(pos));
                        Log.e("leth*******", String.valueOf(array.length()));
                        if (!url.startsWith("http")) {
                            fileUploadUpdate(url, pos, id);
                        } else {
                            images.add(new com.example.digitalcard.pojo.Image(url, "", pos, true));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (array.length() == images.size()) {
                    update_Service(id, seviceName.getText().toString(), currencyType.getText().toString(), serviceamount.getText().toString());
                }

            }
        });


        add_photos_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMultiple_Photos();
            }
        });
        updateSheetDialog.setContentView(updatesheetView);
        updateSheetDialog.setCanceledOnTouchOutside(true);
        updateSheetDialog.show();

    }


    //update Service Push------------------------------------------------------------------------------------------------------------
    private void update_Service(String product_id, String pro_Name, String pro_currenyType, String pro_price) {
        Api apiInterface = NetworkClient.getRetrofitClient(ServiceList.this).create(Api.class);
        Name name = new Name(pro_Name);
        AddServicePojo addServicePojo = new AddServicePojo(name, "", "", images, Integer.parseInt(pro_price), pro_currenyType, 1, true);
        String json = new Gson().toJson(addServicePojo);
        Log.e("JSON", json);
        String url = updateServices + product_id;
        Log.e("url", url);
        Call<ResponseBody> callqr = apiInterface.updateService(url, addServicePojo);
        Log.e("callqr", String.valueOf(callqr));


        callqr.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<okhttp3.ResponseBody> call, Response<ResponseBody> response) {
                try {
                    Functions.cancel_loader();
                    JSONObject jsonObject1 = new JSONObject(response.body().string());
                    Log.e("response##", String.valueOf(jsonObject1));
                    updateSheetDialog.dismiss();
                    HomePage.business_cardDatas(ServiceList.this);
                    new Handler().postDelayed(
                            new Runnable() {
                                @Override
                                public void run() {
                                    serviceListData();
                                }
                            }, 3000);


                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("exe^^^", e.toString());
                }
            }

            @Override
            public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {
                Toast.makeText(ServiceList.this, "Try again Later", Toast.LENGTH_SHORT).show();

            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("data of intent", String.valueOf(data));

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            List<com.esafirm.imagepicker.model.Image> images = ImagePicker.getImages(data);
            // or get a single image only
            imageVideoPathList = new ArrayList<>();

            Log.e("size", String.valueOf(images.size()));
            for (int i = 0; i < images.size(); i++) {
                img_size = images.size();
                String path = images.get(i).getPath();
                Log.e("image_id su", path);
                if (path.endsWith("jpg") || path.endsWith("jpeg") || path.endsWith("png")) {
                    imageVideoPathList.add(path);
                    addImages(path, i);
                }
                Log.e("----path----", String.valueOf(imageVideoPathList));

            }

        }
    }

    private void addImages(String path, int i) {
        final LinearLayout root = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.photos_lay, null);

        ImageView imageView = (ImageView) root.findViewById(R.id.image);
        ImageView close = (ImageView) root.findViewById(R.id.close);
        TextView primary = (TextView) root.findViewById(R.id.primary);

        urlpath = path;
        String filename = path.substring(path.lastIndexOf("/"));
        primary.setText(urlpath);

        imageVideo = imageVideo + 1;
        final int imagePosition = imageVideo;
        Log.e("imagePosition ", "N---ULL---" + imagePosition);
        array.put(urlpath);

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMultiplePhotos.removeView(root);
                String pos = primary.getText().toString();
                Log.e("urlpathofselected", pos);
                removePhoto(pos);


            }
        });


        Glide.with(this).load(path).into(imageView);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(245, 245);
        params.setMargins(0, 0, 0, 0);
        addMultiplePhotos.addView(root, params);

    }

    @SuppressLint("NewApi")
    private void removePhoto(String remove_path) {

        for (int i = 0; i < array.length(); i++) {
            try {
                String tempName = String.valueOf(array.get(i));
                if (tempName.equals(remove_path)) {
                    array.remove(i);
                    images.remove(i);
                    Log.e("deletedFinal", array.toString());
                }
            } catch (Exception e) {
                Log.e("Catch_Photo", e.toString());

            }

            Log.e("deletedFinal1", array.toString());
        }
    }

    private void initViews() {
        add_service = findViewById(R.id.add_service);
        serviceList = findViewById(R.id.serviceList);
        no_data = findViewById(R.id.no_data);

    }


    // AWS Image Add---------------------------------------------------------------------------------------------------
    public void fileUploadAdd(String urlpath, int pos) {
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                ServiceList.this, // Application Context
                "ap-south-1:40815303-b46b-440a-9307-25517efe7c5b", // Identity Pool ID
                Regions.AP_SOUTH_1 // Region enum

        );
        AmazonS3 s3client1 = new AmazonS3Client(credentialsProvider);
        s3client1.setEndpoint("s3-ap-south-1.amazonaws.com");
        s3client1.setS3ClientOptions(S3ClientOptions.builder()
                .setPathStyleAccess(true)
                .disableChunkedEncoding()
                .build());

        TransferUtility transferUtility = new TransferUtility(s3client1, ServiceList.this);
//        String url = "/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20210502-WA0006.jpg";
        String url = urlpath;

        Date now = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("ddMMyyyyHHmmss", Locale.ENGLISH);
        final String currentdatetime = dateformat.format(now);
        Log.e("getcurrentdate", currentdatetime);

        final String filenameofCont = url.substring(url.lastIndexOf("/") + 1);
        Log.e("file+++++++", filenameofCont);
        String extensionRemoved = filenameofCont.split("\\.")[0];
        Log.e("onRemoved+++", extensionRemoved);
        final String lastType = filenameofCont.substring(filenameofCont.lastIndexOf(".") + 1);
        Log.e("lasttype", lastType);
        String mediaName = "media" + currentdatetime;
        final String FileName = HomePage.business_Name + "_" + business_ID + SUFFIX + "Service" + SUFFIX + extensionRemoved + "." + lastType;
        Log.e("filenameofcontent", FileName);
        File fileToUpload = new File(url);

        TransferObserver observer = transferUtility.upload(bucketName, FileName, fileToUpload);
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                switch (state) {
                    case IN_PROGRESS:
                        Log.e("goingOn_______________", "goingon");
                        break;

                    case COMPLETED:
                        Log.e("Completed", "Completed");
                        String publicFileName = "https://" + bucketName + ".s3." + "ap-south-1" + ".amazonaws.com/" + FileName;
                        Log.e("publicFileName+++++++++", publicFileName);

                        images.add(new com.example.digitalcard.pojo.Image(publicFileName, "", pos, true));

                        if (array.length() == images.size()) {
                            add_newService(seviceName.getText().toString(), currencyType.getText().toString(), serviceamount.getText().toString());

                        }
                        break;

                    case FAILED:
                        Log.e("Failed", "Failed");

                        break;

                    case WAITING_FOR_NETWORK:
                        Log.e("network", "network");

                    default:
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                int percentage = (int) (bytesCurrent / bytesTotal * 100);
                //Display percentage transfered to user
                Log.e("percent", String.valueOf(percentage));
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error********", String.valueOf(ex));
            }
        });
    }

    // AWS Image Update---------------------------------------------------------------------------------------------------
    public void fileUploadUpdate(String urlpath, int pos, String business_id) {
        CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                ServiceList.this, // Application Context
                "ap-south-1:40815303-b46b-440a-9307-25517efe7c5b", // Identity Pool ID
                Regions.AP_SOUTH_1 // Region enum

        );
        AmazonS3 s3client1 = new AmazonS3Client(credentialsProvider);
        s3client1.setEndpoint("s3-ap-south-1.amazonaws.com");
        s3client1.setS3ClientOptions(S3ClientOptions.builder()
                .setPathStyleAccess(true)
                .disableChunkedEncoding()
                .build());

        TransferUtility transferUtility = new TransferUtility(s3client1, ServiceList.this);
//        String url = "/storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20210502-WA0006.jpg";
        String url = urlpath;

        Date now = new Date();
        SimpleDateFormat dateformat = new SimpleDateFormat("ddMMyyyyHHmmss", Locale.ENGLISH);
        final String currentdatetime = dateformat.format(now);
        Log.e("getcurrentdate", currentdatetime);

        final String filenameofCont = url.substring(url.lastIndexOf("/") + 1);
        Log.e("file+++++++", filenameofCont);
        String extensionRemoved = filenameofCont.split("\\.")[0];
        Log.e("onRemoved+++", extensionRemoved);
        final String lastType = filenameofCont.substring(filenameofCont.lastIndexOf(".") + 1);
        Log.e("lasttype", lastType);
        String mediaName = "media" + currentdatetime;
        final String FileName = HomePage.business_Name + "_" + business_ID + SUFFIX + "Service" + SUFFIX + extensionRemoved + "." + lastType;
        Log.e("filenameofcontent", FileName);
        File fileToUpload = new File(url);

        TransferObserver observer = transferUtility.upload(bucketName, FileName, fileToUpload);
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                switch (state) {
                    case IN_PROGRESS:
                        Log.e("goingOn_______________", "goingon");
                        break;

                    case COMPLETED:
                        Log.e("Completed", "Completed");
                        String publicFileName = "https://" + bucketName + ".s3." + "ap-south-1" + ".amazonaws.com/" + FileName;
                        Log.e("publicFileName+++++++++", publicFileName);

                        images.add(new com.example.digitalcard.pojo.Image(publicFileName, "", pos, true));

                        if (array.length() == images.size()) {
                            update_Service(business_id, seviceName.getText().toString(), currencyType.getText().toString(), serviceamount.getText().toString());
                        }
                        break;

                    case FAILED:
                        Log.e("Failed", "Failed");

                        break;

                    case WAITING_FOR_NETWORK:
                        Log.e("network", "network");

                    default:
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                int percentage = (int) (bytesCurrent / bytesTotal * 100);
                //Display percentage transfered to user
                Log.e("percent", String.valueOf(percentage));
            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("error********", String.valueOf(ex));
            }
        });
    }

}