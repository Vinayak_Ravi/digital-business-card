package com.example.digitalcard.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.digitalcard.R;

/**
 *Created by Vinayak Ravi 20/04/2021
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}