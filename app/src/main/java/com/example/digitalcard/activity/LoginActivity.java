package com.example.digitalcard.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.digitalcard.R;
import com.example.digitalcard.receivers.OTP_Receiver;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.hbb20.CountryCodePicker;

import java.util.Objects;


/**
 * Created by Vinayak Ravi 20/04/2021
 */

public class LoginActivity extends AppCompatActivity {

    CountryCodePicker countryCodePicker;
    String strcountryCode;
    Animation fade_anim;
    Animation slide_in_from_left;
    Animation slide_in_from_right;
    ConstraintLayout numberLay;
    CardView sumbitNumber;
    BottomSheetDialog bottomSheetDialog;
    EditText mobileNumber;
    TextView success;
    OTP_Receiver receiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#3ABEF0"));
        }

        initView();


        countryCodePicker.getDefaultCountryCode();
        strcountryCode = countryCodePicker.getDefaultCountryCode();
        countryCodePicker.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                strcountryCode = countryCodePicker.getSelectedCountryCode();

            }
        });

        sumbitNumber.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {

                if (mobileNumber.getText().toString() != null && mobileNumber.getText().toString().length() == 10) {
                    getOtp(mobileNumber.getText().toString());
                } else {
                    mobileNumber.setError("Please provide correct number");
                    mobileNumber.setFocusable(true);
                }
            }
        });


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getOtp(String number) {
        TextView number_text, resend_text;
        EditText one, two, three, four;
        ConstraintLayout sumbit, otpLay;


        bottomSheetDialog = new BottomSheetDialog(LoginActivity.this, R.style.BottomSheetTheme);

        View sheetView = LayoutInflater.from(getApplicationContext()).inflate(R.layout.otp_layout, (ViewGroup) findViewById(R.id.bottomSheet));

        number_text = sheetView.findViewById(R.id.number_text);
        otpLay = sheetView.findViewById(R.id.otpLay);
        sumbit = sheetView.findViewById(R.id.sumbit);
        one = sheetView.findViewById(R.id.one_otp);
        two = sheetView.findViewById(R.id.two_otp);
        three = sheetView.findViewById(R.id.three_otp);
        four = sheetView.findViewById(R.id.four_otp);

        //OTP Receiver-------------------------
        receiver = new OTP_Receiver();
        receiver.setEditText(one);
        receiver.setEditText_two(two);
        receiver.setEditText_three(three);
        receiver.setEditText_four(four);

        requestSMSPermission();


        //Manual TYping OTP----------------------------------------


        one.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (one.getText().length() == 1) {
                    two.requestFocus();
                    two.setCursorVisible(true);
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {


            }

            public void afterTextChanged(Editable s) {


            }
        });
        two.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (two.getText().length() == 1) {
                    three.requestFocus();
                    three.setCursorVisible(true);
                } else if (two.getText().length() == 0) {
                    one.requestFocus();
                    one.setCursorVisible(true);
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                if (two.getText().length() == 0) {
                    one.requestFocus();
                    one.setCursorVisible(true);
                }

            }

            public void afterTextChanged(Editable s) {


            }
        });
        three.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (three.getText().length() == 1) {
                    four.requestFocus();
                    four.setCursorVisible(true);
                } else if (three.getText().length() == 0) {
                    two.requestFocus();
                    two.setCursorVisible(true);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {


            }

            public void afterTextChanged(Editable s) {

            }
        });
        four.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (four.getText().length() == 1) {
                    sumbit.callOnClick();

                } else if (four.getText().length() == 0) {
                    three.requestFocus();
                    three.setCursorVisible(true);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {


            }

            public void afterTextChanged(Editable s) {

            }
        });


        otpLay.startAnimation(slide_in_from_right);
        sumbit.startAnimation(fade_anim);
        resend_text = sheetView.findViewById(R.id.resend_text);
        number_text.setText("Code is sent to +" + strcountryCode + " " + number);
        Typeface typeface = ResourcesCompat.getFont(this, R.font.montserrat_alternates);

        String tmpHtml = "Didn't receive the code? \n <font color='#00000'> <b>Resend OTP</b></font>";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            resend_text.setText(Html.fromHtml(tmpHtml, Html.FROM_HTML_MODE_COMPACT));
        } else {
            resend_text.setText(Html.fromHtml(tmpHtml));
        }

        sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = one.getText().toString() + two.getText().toString() + three.getText().toString() + four.getText().toString();
                if (otp != null && otp.length() == 4) {
                    Dialog dialog;
                    dialog = new Dialog(LoginActivity.this);
                    dialog.setContentView(R.layout.dialog_layout);
                    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    success = dialog.findViewById(R.id.btnContinue);

                    success.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
                            i.putExtra("PhoneNumber", number);
                            i.putExtra("code", strcountryCode);
                            startActivity(i);
                        }
                    });

                    dialog.setCanceledOnTouchOutside(false);
                    dialog.setCancelable(false);
                    dialog.show();
                } else {

                }

            }
        });

        number_text.setTypeface(typeface);
        resend_text.setTypeface(typeface);

        bottomSheetDialog.setContentView(sheetView);
        bottomSheetDialog.setCanceledOnTouchOutside(false);
        bottomSheetDialog.show();

    }

    private void initView() {

        countryCodePicker = findViewById(R.id.ccp);
        numberLay = findViewById(R.id.numberLay);
        mobileNumber = findViewById(R.id.mobileNumber);
        sumbitNumber = findViewById(R.id.sumbitNumber);
        fade_anim = AnimationUtils.loadAnimation(this, R.anim.fade_anim);
        slide_in_from_left = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_left);
        slide_in_from_right = AnimationUtils.loadAnimation(this, R.anim.slide_in_from_right);


    }


    private void requestSMSPermission() {
        String permission = Manifest.permission.RECEIVE_SMS;

        int grant = ContextCompat.checkSelfPermission(this, permission);
        if (grant != PackageManager.PERMISSION_GRANTED) {
            String[] permission_list = new String[1];
            permission_list[0] = permission;

            ActivityCompat.requestPermissions(this, permission_list, 1);
        }
    }
}