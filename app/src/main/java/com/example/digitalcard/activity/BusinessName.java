package com.example.digitalcard.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.digitalcard.R;
import com.example.digitalcard.pojo.CreateUserRequest;
import com.example.digitalcard.utils.Api;
import com.example.digitalcard.utils.Functions;
import com.example.digitalcard.utils.NetworkClient;
import com.example.digitalcard.utils.Variables;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * Created by Vinayak Ravi 20/04/2021
 */

public class BusinessName extends AppCompatActivity {
    EditText businessName;
    MaterialButton sumbit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_name);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.parseColor("#3ABEF0"));
        }

        businessName = findViewById(R.id.businessName);
        sumbit = findViewById(R.id.sumbit);
        sumbit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!businessName.getText().toString().equals("")) {
                    createBusiness(businessName.getText().toString());
                } else {
                    businessName.setError("Please enter Business Name");
                    businessName.setFocusable(true);
                }

            }
        });

    }

    private void createBusiness(String business_name) {
        Api apiInterface = NetworkClient.getRetrofitClient(BusinessName.this).create(Api.class);

        SharedPreferences prefs = getSharedPreferences("login", MODE_PRIVATE);
        String token = prefs.getString(Variables.token, "");
        Log.e("tokena888888888", token);
        Call<okhttp3.ResponseBody> callqr = apiInterface.createBusinessCard(business_name);
        Log.e("callqr", String.valueOf(callqr));

        callqr.enqueue(new Callback<okhttp3.ResponseBody>() {
            @Override
            public void onResponse(Call<okhttp3.ResponseBody> call, retrofit2.Response<okhttp3.ResponseBody> response) {
                try {
                    Functions.cancel_loader();


                    JSONObject jsonObject1 = new JSONObject(response.body().string());
                    Log.e("response##", String.valueOf(jsonObject1));
                    JSONObject jsonObject2 = jsonObject1.getJSONObject("data");
                    String businessID = jsonObject2.getString("_id");
                    String businessName = jsonObject2.getString("businessName");
                    Log.e("businessID",businessID);
                    SharedPreferences.Editor editor = getSharedPreferences("login", MODE_PRIVATE).edit();
                    editor.putString(Variables.createBusiness, "0");
                    editor.putString(Variables.businessId, businessID);
                    editor.putString(Variables.businessName, businessName);
                    editor.apply();

                    finish();

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("exe^^^", e.toString());
                }
            }

            @Override
            public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {
                Toast.makeText(BusinessName.this, "Try again Later", Toast.LENGTH_SHORT).show();

            }
        });
    }
}