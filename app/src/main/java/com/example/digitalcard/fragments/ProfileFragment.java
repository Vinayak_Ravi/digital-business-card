package com.example.digitalcard.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.example.digitalcard.R;
import com.example.digitalcard.activity.ContactUs;
import com.example.digitalcard.activity.HomePage;
import com.example.digitalcard.activity.SettingsActivity;

import static com.example.digitalcard.utils.Functions.hideKeyboard;

/**
 * Created by Vinayak Ravi 20/04/2021
 */

public class ProfileFragment extends Fragment {
    TextView settings, contact;
    EditText userName, mailID, phoneNumber;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        settings = view.findViewById(R.id.settings);
        contact = view.findViewById(R.id.contact);
        userName = view.findViewById(R.id.userName);
        mailID = view.findViewById(R.id.mailID);
        phoneNumber = view.findViewById(R.id.phoneNumber);

        userName.setText(HomePage.f_name + " " + HomePage.l_name);
        phoneNumber.setText(HomePage.mobile);
        mailID.setText(HomePage.e_mail);


        phoneNumber.setOnTouchListener(hideKeyboard);
        mailID.setOnTouchListener(hideKeyboard);
        userName.setOnTouchListener(hideKeyboard);


        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), SettingsActivity.class);
                startActivity(i);
            }
        });
        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ContactUs.class);
                startActivity(i);
            }
        });

        return view;
    }
}