package com.example.digitalcard.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.digitalcard.R;
import com.example.digitalcard.activity.ProductList;
import com.example.digitalcard.adapter.AddBusinessAdapter;
import com.example.digitalcard.adapter.AllProductListAdapter;
import com.example.digitalcard.pojo.AddBusinessModel;
import com.example.digitalcard.pojo.AllProductListModel;

import java.util.ArrayList;
import java.util.List;

/**
 *Created by Vinayak Ravi 20/04/2021
 */

public class AddBusiness extends Fragment {
    RecyclerView businessList;
    AddBusinessAdapter addBusinessAdapter;
    private List<AddBusinessModel> addBusinessModels = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_add_business, container, false);

        businessList = view.findViewById(R.id.businessList);


        addBusinessAdapter = new AddBusinessAdapter(addBusinessModels, getContext());
        businessList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        businessList.setNestedScrollingEnabled(false);
        businessList.setHasFixedSize(false);
        businessList.setAdapter(addBusinessAdapter);
        addBusinessAdapter.notifyDataSetChanged();
        return view;

    }
}