package com.example.digitalcard.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.os.StrictMode;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.bumptech.glide.Glide;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;
import com.example.digitalcard.R;
import com.example.digitalcard.activity.AddAddress;
import com.example.digitalcard.activity.HomePage;
import com.example.digitalcard.activity.RegistrationActivity;
import com.example.digitalcard.pojo.AboutUs;
import com.example.digitalcard.pojo.BusinessDetailPojo;
import com.example.digitalcard.pojo.NatureOfBusiness;
import com.example.digitalcard.utils.Api;
import com.example.digitalcard.utils.Functions;
import com.example.digitalcard.utils.NetworkClient;
import com.example.digitalcard.utils.Variables;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


import static android.app.Activity.RESULT_OK;
import static com.example.digitalcard.activity.HomePage.business_ID;
import static com.example.digitalcard.activity.HomePage.user_id;
import static com.example.digitalcard.utils.ApiUrls.about_Us;
import static com.example.digitalcard.utils.ApiUrls.addSocialLinks;
import static com.example.digitalcard.utils.ApiUrls.updateSocialLinks;
import static com.example.digitalcard.utils.Functions.hideKeyboard;
import static com.example.digitalcard.utils.Variables.AboutusObject;
import static com.example.digitalcard.utils.Variables.bucketName;

/**
 * Created by Vinayak Ravi 20/04/2021
 */


public class BusinessDetail extends Fragment {
    private static final String SUFFIX = "/";

    EditText businessName, aboutCompany, designation, phone, mailId, website, since, natureBusiness_name;
    ImageView addPhoto;
    CircleImageView displayImg;
    BottomSheetDialog bottomSheetDialog;
    BottomSheetDialog bottomSheetDialog1;
    public static List<String> imageVideoPathList;
    int img_size = 0;
    private View fragmentView;
    RelativeLayout addAddress;
    FloatingActionButton social_link;
    ImageView facebook, twitter, whatsapp, linkedin, youtube, instagram;
    TextView social_media, siteUrl;
    EditText siteData;
    ConstraintLayout saveData;
    CardView saveSocial;
    String updatSocial_id;
    AmazonS3Client s3client;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (fragmentView != null) {
            return fragmentView;
        }
        View view = inflater.inflate(R.layout.fragment_business_detail, container, false);
        fragmentView = view;
        businessName = view.findViewById(R.id.business_name);
        designation = view.findViewById(R.id.designation);
        phone = view.findViewById(R.id.phone);
        mailId = view.findViewById(R.id.mailId);
        website = view.findViewById(R.id.website);
        natureBusiness_name = view.findViewById(R.id.natureBusiness_name);
        addPhoto = view.findViewById(R.id.addPhoto);
        since = view.findViewById(R.id.since);
        aboutCompany = view.findViewById(R.id.aboutCompany);
        displayImg = view.findViewById(R.id.displayImg);
        addAddress = view.findViewById(R.id.addAddress);
        social_link = view.findViewById(R.id.social_link);
        saveData = view.findViewById(R.id.saveData);

        aboutCompany.setMovementMethod(new ScrollingMovementMethod());
        aboutCompany.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (aboutCompany.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            }
        });

        saveData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!businessName.getText().toString().equals("") && !aboutCompany.getText().toString().equals("") && !phone.getText().toString().equals("") && !mailId.getText().toString().equals("") && !designation.getText().toString().equals("") && !since.getText().toString().equals("") && !natureBusiness_name.getText().toString().equals("")) {
                    Functions.Show_loader(getActivity(), false, true);
                    hideKeyboard(getActivity());
                    updateDetails(businessName.getText().toString(), aboutCompany.getText().toString(), phone.getText().toString(), mailId.getText().toString(), website.getText().toString(), designation.getText().toString(), since.getText().toString(), natureBusiness_name.getText().toString());
                } else {
                    if (businessName.getText().toString().equals("")) {
                        businessName.setError("Enter Business Name");
                        businessName.setFocusable(true);
                    }
                    if (aboutCompany.getText().toString().equals("")) {
                        aboutCompany.setError("Enter About Company");
                        aboutCompany.setFocusable(true);
                    }
                    if (phone.getText().toString().equals("")) {
                        phone.setError("Enter Phone Number");
                        phone.setFocusable(true);
                    }
//                    if (phone.getText().toString().length() != 10) {
//
//                    }
                    if (mailId.getText().toString().equals("")) {
                        mailId.setError("Enter Mail");
                        mailId.setFocusable(true);
                    }
//                    if (website.getText().toString().equals("")){
//                        website.setError("Enter Website");
//                        website.setFocusable(true);
//                    }
                    if (designation.getText().toString().equals("")) {
                        designation.setError("Enter Designation");
                        designation.setFocusable(true);
                    }
                    if (since.getText().toString().equals("")) {
                        since.setError("Enter Since");
                        since.setFocusable(true);
                    }
                    if (natureBusiness_name.getText().toString().equals("")) {
                        natureBusiness_name.setError("Enter Nature of Business");
                        natureBusiness_name.setFocusable(true);
                    }
                }
            }
        });
        social_link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog = new BottomSheetDialog(getContext(), R.style.BottomSheetTheme);
                View sheetView = LayoutInflater.from(getContext()).inflate(R.layout.sociallinks, (ViewGroup) view.findViewById(R.id.bottomSheet));

                facebook = sheetView.findViewById(R.id.facebook);
                twitter = sheetView.findViewById(R.id.twitter);
                whatsapp = sheetView.findViewById(R.id.whatsapp);
                youtube = sheetView.findViewById(R.id.youtube);
                linkedin = sheetView.findViewById(R.id.linkedin);
                instagram = sheetView.findViewById(R.id.instagram);

                bottomSheetDialog1 = new BottomSheetDialog(getContext(), R.style.BottomSheetTheme);
                View sheetView1 = LayoutInflater.from(getContext()).inflate(R.layout.addlinks, (ViewGroup) view.findViewById(R.id.bottomSheet));
                social_media = sheetView1.findViewById(R.id.social_media);
                siteUrl = sheetView1.findViewById(R.id.siteUrl);
                siteData = sheetView1.findViewById(R.id.siteData);
                saveSocial = sheetView1.findViewById(R.id.saveSocial);
                bottomSheetDialog1.setContentView(sheetView1);
                bottomSheetDialog1.setCanceledOnTouchOutside(false);
                saveSocial.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String name = social_media.getText().toString();
                        String data = siteData.getText().toString();
                        if (updatSocial_id != null && !updatSocial_id.equals("")) {
                            Toast.makeText(getActivity(), updatSocial_id, Toast.LENGTH_SHORT).show();
                            updateSocailData(name, data, updatSocial_id);
                        } else {
                            socailData(name, data);
                        }
                    }
                });
                facebook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        siteData.getText().clear();
                        try {
                            JSONArray jsonArray = new JSONArray(Variables.SocialMediaDatas);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String name = object.getString("name");
                                if (name != null && name.equals("Facebook")) {
                                    updatSocial_id = object.getString("_id");
                                    siteData.setText(object.getString("link"));


                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        siteUrl.setText("www.facebook.com/");
                        social_media.setText("Facebook");

                        bottomSheetDialog1.show();
                    }
                });
                instagram.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        siteData.getText().clear();

                        try {
                            JSONArray jsonArray = new JSONArray(Variables.SocialMediaDatas);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String name = object.getString("name");
                                if (name != null && name.equals("Instagram")) {
                                    updatSocial_id = object.getString("_id");
                                    siteData.setText(object.getString("link"));


                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        siteUrl.setText("www.instagram.com/");
                        social_media.setText("Instagram");

                        bottomSheetDialog1.show();
                    }
                });
                twitter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        siteData.getText().clear();

                        try {
                            JSONArray jsonArray = new JSONArray(Variables.SocialMediaDatas);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String name = object.getString("name");
                                if (name != null && name.equals("Twitter")) {
                                    updatSocial_id = object.getString("_id");
                                    siteData.setText(object.getString("link"));


                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        siteUrl.setText("www.twitter.com/");
                        social_media.setText("Twitter");

                        bottomSheetDialog1.show();
                    }
                });
                linkedin.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        siteData.getText().clear();

                        try {
                            JSONArray jsonArray = new JSONArray(Variables.SocialMediaDatas);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String name = object.getString("name");
                                if (name != null && name.equals("LinkedIn")) {
                                    updatSocial_id = object.getString("_id");
                                    siteData.setText(object.getString("link"));


                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        siteUrl.setText("www.linkedin.com/");
                        social_media.setText("LinkedIn");

                        bottomSheetDialog1.show();
                    }
                });
                youtube.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        siteData.getText().clear();

                        try {
                            JSONArray jsonArray = new JSONArray(Variables.SocialMediaDatas);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String name = object.getString("name");
                                if (name != null && name.equals("YouTube")) {
                                    updatSocial_id = object.getString("_id");
                                    siteData.setText(object.getString("link"));


                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        siteUrl.setText("www.youtube.com/");
                        social_media.setText("YouTube");

                        bottomSheetDialog1.show();
                    }
                });
                whatsapp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        siteData.getText().clear();

                        try {
                            JSONArray jsonArray = new JSONArray(Variables.SocialMediaDatas);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject object = jsonArray.getJSONObject(i);
                                String name = object.getString("name");
                                if (name != null && name.equals("Whatsapp")) {
                                    updatSocial_id = object.getString("_id");
                                    siteData.setText(object.getString("link"));

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        siteUrl.setText("www.whatsapp.com/");
                        social_media.setText("Whatsapp");

                        bottomSheetDialog1.show();
                    }
                });

                bottomSheetDialog.setContentView(sheetView);
                bottomSheetDialog.setCanceledOnTouchOutside(false);
                bottomSheetDialog.show();
            }
        });

        addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), AddAddress.class);
                startActivity(i);
            }
        });

        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConstraintLayout addPhoto, removePhoto;
                bottomSheetDialog = new BottomSheetDialog(getContext(), R.style.BottomSheetTheme);

                View sheetView = LayoutInflater.from(getContext()).inflate(R.layout.addphoto_bottomsheet, (ViewGroup) view.findViewById(R.id.bottomSheet));

                addPhoto = sheetView.findViewById(R.id.addPhoto);
                removePhoto = sheetView.findViewById(R.id.removePhoto);

                addPhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ImagePicker.create(getActivity()) // Activity or Fragment
                                .includeVideo(true)
                                .limit(1)
                                .folderMode(true)
                                .toolbarFolderTitle("Select Folder") // folder selection title
                                .multi()
                                .theme(R.style.PickerTheme) // must inherit ef_BaseTheme. please refer to sample
                                .enableLog(false) // disabling log
                                .start();
                    }
                });
                removePhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                bottomSheetDialog.setContentView(sheetView);
                bottomSheetDialog.setCanceledOnTouchOutside(false);
                bottomSheetDialog.show();
            }
        });
        businessName.setText(HomePage.business_Name);
        businessDetailData();
        return view;

    }

    private void updateSocailData(String name, String data, String id) {
        Api apiInterface = NetworkClient.getRetrofitClient(getActivity()).create(Api.class);
        String url = updateSocialLinks + id;
        Log.e("url", url);
        Call<okhttp3.ResponseBody> callqr = apiInterface.updatesocialLink(url, name, data, true);
        Log.e("callqr", String.valueOf(callqr));


        callqr.enqueue(new Callback<okhttp3.ResponseBody>() {
            @Override
            public void onResponse(Call<okhttp3.ResponseBody> call, Response<okhttp3.ResponseBody> response) {
                try {
                    Functions.cancel_loader();
                    JSONObject jsonObject1 = new JSONObject(response.body().string());
                    Log.e("response##", String.valueOf(jsonObject1));
                    HomePage.business_cardDatas(getActivity());

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("exe^^^", e.toString());
                }
            }

            @Override
            public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {
                Toast.makeText(getContext(), "Try again Later", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void socailData(String name, String data) {
        Api apiInterface = NetworkClient.getRetrofitClient(getActivity()).create(Api.class);
        String url = addSocialLinks + business_ID;
        Log.e("url", url);
        Call<okhttp3.ResponseBody> callqr = apiInterface.socialLink(url, name, data, true);
        Log.e("callqr", String.valueOf(callqr));


        callqr.enqueue(new Callback<okhttp3.ResponseBody>() {
            @Override
            public void onResponse(Call<okhttp3.ResponseBody> call, Response<okhttp3.ResponseBody> response) {
                try {
                    Functions.cancel_loader();
                    JSONObject jsonObject1 = new JSONObject(response.body().string());
                    Log.e("response##", String.valueOf(jsonObject1));
                    HomePage.business_cardDatas(getActivity());

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("exe^^^", e.toString());
                }
            }

            @Override
            public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {
                Toast.makeText(getContext(), "Try again Later", Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void businessDetailData() {
        String data = AboutusObject;
//        Log.e("data",data);
        try {
            JSONObject main = new JSONObject(data);
            String description = main.getString("description");
            String _since = main.getString("since");
            JSONObject natureOfBusiness = main.getJSONObject("natureOfBusiness");
            String business = natureOfBusiness.getString("en");
            aboutCompany.setText(description);
            since.setText(_since);
            natureBusiness_name.setText(business);


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void updateDetails(String businessName, String aboutCompany, String mobile, String mail, String website, String designation, String since, String natureBusiness) {
        Api apiInterface = NetworkClient.getRetrofitClient(getActivity()).create(Api.class);

        NatureOfBusiness natureOfBusiness = new NatureOfBusiness(natureBusiness);
        AboutUs aboutUs = new AboutUs(Integer.parseInt(since), natureOfBusiness, aboutCompany);
        BusinessDetailPojo businessDetailPojo = new BusinessDetailPojo(businessName, "", designation, aboutUs);
        Gson gson = new Gson();
        String json = gson.toJson(businessDetailPojo);
        Log.e("datas^^^^^^^^^^", json);
        Toast.makeText(getContext(), user_id, Toast.LENGTH_SHORT).show();
        String url = about_Us + business_ID;
        Log.e("url", url);

        Call<okhttp3.ResponseBody> callqr = apiInterface.aboutUs(business_ID, businessDetailPojo);
        Log.e("callqr", String.valueOf(callqr));


        callqr.enqueue(new Callback<okhttp3.ResponseBody>() {
            @Override
            public void onResponse(Call<okhttp3.ResponseBody> call, Response<okhttp3.ResponseBody> response) {
                try {
                    Functions.cancel_loader();
                    JSONObject jsonObject1 = new JSONObject(response.body().string());
                    Log.e("response##", String.valueOf(jsonObject1));
                    HomePage.business_cardDatas(getActivity());

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("exe^^^", e.toString());
                }
            }

            @Override
            public void onFailure(Call<okhttp3.ResponseBody> call, Throwable t) {
                Toast.makeText(getContext(), "Try again Later", Toast.LENGTH_SHORT).show();

            }
        });


    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("data of intent", String.valueOf(data));

        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            // Get a list of picked images
            List<Image> images = ImagePicker.getImages(data);
            Log.e("REsult***********", images.toString());
            // or get a single image only

            Log.e("size", String.valueOf(images.size()));
            for (int i = 0; i < images.size(); i++) {
                img_size = images.size();

                String path = images.get(i).getPath();
                Log.e("image_id su", path);
                Glide.with(getActivity()).load(path).placeholder(R.drawable.test).into(displayImg);
                bottomSheetDialog.dismiss();


            }
//
        }
    }

   /* @SuppressLint("StaticFieldLeak")
    private class InitTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {

            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

//            credentials = new BasicAWSCredentials(key, secret);

            ClientConfiguration clientConfiguration = new ClientConfiguration();
            clientConfiguration.setMaxErrorRetry(3);
            clientConfiguration.setConnectionTimeout(500 * 1000);
            clientConfiguration.setSocketTimeout(500 * 1000);
            clientConfiguration.setProtocol(Protocol.HTTPS);

            // Initialize the Amazon Cognito credentials provider
            CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
                   getActivity(), // Application Context
                    "us-west-2:365a3749-2889-46a8-aeb1-befefd35aa21", // Identity Pool ID
                    Regions.DEFAULT_REGION // Region enum
            );

            s3client = new AmazonS3Client(credentialsProvider, clientConfiguration);
            s3client.setEndpoint("s3-us-west-2.amazonaws.com");

            return null;
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class PostTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... params) {
//                 mediaAddprogress.setMessage(getString(R.string.Uploading));
//                mediaAddprogress.setCancelable(false);
//                mediaAddprogress.show();
            String url = params[0];
            int mediaCount = Integer.valueOf(params[1]);

            try {

                Date now = new Date();
                SimpleDateFormat dateformat = new SimpleDateFormat("ddMMyyyyHHmmss", Locale.ENGLISH);
                final String currentdatetime = dateformat.format(now);
                Log.e("getcurrentdate", currentdatetime);

                final String filenameofCont = url.substring(url.lastIndexOf("/") + 1);
                final String lastType = filenameofCont.substring(filenameofCont.lastIndexOf(".") + 1);
                Log.e("lasttype", lastType);
                String mediaName = "media" + currentdatetime;
                final String FileName = "business" + SUFFIX + currentdatetime + SUFFIX + mediaName + "." + lastType;
                Log.e("filenameofcontent", FileName);

                File fileToUpload = new File(url);

                long fileLength = fileToUpload.length();
                Log.e("fileToUpload", "  -- " + fileToUpload.length());

                PutObjectRequest putRequest = new PutObjectRequest(bucketName, FileName, fileToUpload)
//                        .withGeneralProgressListener(listener)
                        .withCannedAcl(CannedAccessControlList.PublicRead);
                ;
                PutObjectResult putResponse = s3client.putObject(putRequest);

                final String ImagevideoURL = String.valueOf(s3client.getUrl(
                        bucketName, //The S3 Bucket To Upload To
                        FileName));

                String[] stArray = new String[4];
                stArray[0] = mediaName + "." + lastType;
                stArray[1] = currentdatetime;
                stArray[2] = lastType;
                stArray[3] = ImagevideoURL;

                Log.e("mediaCount -=---------", "" + mediaCount);

                int m = attachmentviamobile(stArray);

                if (m <= 1) {
                    if (mediaCount == imageVideoPathList.size() - 1) {
                        if (mediaAddprogress != null) {


                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return "All Done!";
        }

    }
*/

}