package com.example.digitalcard.fragments;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.digitalcard.R;
import com.example.digitalcard.activity.ProductList;
import com.example.digitalcard.activity.ServiceList;
import com.example.digitalcard.adapter.ProductListAdapter;
import com.example.digitalcard.adapter.ServiceListAdapter;
import com.example.digitalcard.pojo.ProductListModel;
import com.example.digitalcard.pojo.AllServiceListModel;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vinayak Ravi 20/04/2021
 */

public class HomeFragment extends Fragment {

    TextView productView, serviceView;
    RecyclerView product_list, service_list;
    private ProductListAdapter productListAdapter;
    private ServiceListAdapter serviceListAdapter;
    private List<ProductListModel> productListModels = new ArrayList<>();
    private List<AllServiceListModel> serviceListModels = new ArrayList<>();
    MaterialButton addProducts, addService;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        productView = view.findViewById(R.id.productView);
        service_list = view.findViewById(R.id.service_list);
        serviceView = view.findViewById(R.id.serviceView);
        product_list = view.findViewById(R.id.product_list);
        addProducts = view.findViewById(R.id.addProducts);
        addService = view.findViewById(R.id.addService);


        String tmpHtml = "<font color='#3ABEF0'> <u>view more</u></font>";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            productView.setText(Html.fromHtml(tmpHtml, Html.FROM_HTML_MODE_COMPACT));
            serviceView.setText(Html.fromHtml(tmpHtml, Html.FROM_HTML_MODE_COMPACT));
        } else {
            productView.setText(Html.fromHtml(tmpHtml));
            serviceView.setText(Html.fromHtml(tmpHtml));
        }


        productView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ProductList.class);
//                ActivityOptions options = ActivityOptions
//                        .makeSceneTransitionAnimation(this, androidRobotView, "robot");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    startActivity(i, ActivityOptions.makeSceneTransitionAnimation(getActivity()).toBundle());
                }

            }
        });

        addProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), ProductList.class);
                i.putExtra("add", "add");
                startActivity(i);
            }
        });

        addService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), ServiceList.class);
                i.putExtra("add", "add");
                startActivity(i);
            }
        });

        serviceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), ServiceList.class);
                startActivity(i);
            }
        });


        productListAdapter = new ProductListAdapter(productListModels, getActivity());
        product_list.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        product_list.setNestedScrollingEnabled(false);
        product_list.setHasFixedSize(false);
        product_list.setAdapter(productListAdapter);
        productListAdapter.notifyDataSetChanged();


        serviceListAdapter = new ServiceListAdapter(serviceListModels, getActivity());
        service_list.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        service_list.setNestedScrollingEnabled(false);
        service_list.setHasFixedSize(false);
        service_list.setAdapter(serviceListAdapter);
        serviceListAdapter.notifyDataSetChanged();


        return view;
    }

}