package com.example.digitalcard.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Image {


    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("caption")
    @Expose
    private String caption;
    @SerializedName("listingOrder")
    @Expose
    private Integer listingOrder;
    @SerializedName("isVisible")
    @Expose
    private Boolean isVisible;

    public Image( String link, String caption, Integer listingOrder, Boolean isVisible) {
        this.link = link;
        this.caption = caption;
        this.listingOrder = listingOrder;
        this.isVisible = isVisible;
    }


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Integer getListingOrder() {
        return listingOrder;
    }

    public void setListingOrder(Integer listingOrder) {
        this.listingOrder = listingOrder;
    }

    public Boolean getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(Boolean isVisible) {
        this.isVisible = isVisible;
    }

}
