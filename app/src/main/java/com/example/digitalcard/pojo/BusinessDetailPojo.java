package com.example.digitalcard.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusinessDetailPojo {

    @SerializedName("businessName")
    @Expose
    private String businessName;
    @SerializedName("businessLogo")
    @Expose
    private String businessLogo;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("aboutUs")
    @Expose
    private AboutUs aboutUs;

    public BusinessDetailPojo(String businessName, String businessLogo, String role, AboutUs aboutUs) {
        this.businessName = businessName;
        this.businessLogo = businessLogo;
        this.role = role;
        this.aboutUs = aboutUs;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessLogo() {
        return businessLogo;
    }

    public void setBusinessLogo(String businessLogo) {
        this.businessLogo = businessLogo;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public AboutUs getAboutUs() {
        return aboutUs;
    }

    public void setAboutUs(AboutUs aboutUs) {
        this.aboutUs = aboutUs;
    }

}
