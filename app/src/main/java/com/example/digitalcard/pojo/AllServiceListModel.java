package com.example.digitalcard.pojo;

public class AllServiceListModel {
    String _id;
    String productName;
    String produuctCurrencytype;
    String productPrice;
    String images;

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProduuctCurrencytype() {
        return produuctCurrencytype;
    }

    public void setProduuctCurrencytype(String produuctCurrencytype) {
        this.produuctCurrencytype = produuctCurrencytype;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }
}
