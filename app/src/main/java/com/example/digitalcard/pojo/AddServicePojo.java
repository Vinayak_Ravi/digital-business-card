package com.example.digitalcard.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddServicePojo {
    @SerializedName("name")
    @Expose
    private Name name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("images")
    @Expose
    private List<Image> images = null;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("listingOrder")
    @Expose
    private Integer listingOrder;
    @SerializedName("isVisible")
    @Expose
    private Boolean isVisible;

    public AddServicePojo(Name name, String description, String note, List<Image> images, Integer price, String currency, Integer listingOrder, Boolean isVisible) {
        this.name = name;
        this.description = description;
        this.note = note;
        this.images = images;
        this.price = price;
        this.currency = currency;
        this.listingOrder = listingOrder;
        this.isVisible = isVisible;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getListingOrder() {
        return listingOrder;
    }

    public void setListingOrder(Integer listingOrder) {
        this.listingOrder = listingOrder;
    }

    public Boolean getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(Boolean isVisible) {
        this.isVisible = isVisible;
    }
}
