package com.example.digitalcard.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AboutUs {

    @SerializedName("since")
    @Expose
    private Integer since;
    @SerializedName("natureOfBusiness")
    @Expose
    private NatureOfBusiness natureOfBusiness;
    @SerializedName("description")
    @Expose
    private String description;

    public AboutUs(Integer since, NatureOfBusiness natureOfBusiness, String description) {
        this.since = since;
        this.natureOfBusiness = natureOfBusiness;
        this.description = description;
    }

    public Integer getSince() {
        return since;
    }

    public void setSince(Integer since) {
        this.since = since;
    }

    public NatureOfBusiness getNatureOfBusiness() {
        return natureOfBusiness;
    }

    public void setNatureOfBusiness(NatureOfBusiness natureOfBusiness) {
        this.natureOfBusiness = natureOfBusiness;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}