package com.example.digitalcard.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.EditText;


public class OTP_Receiver extends BroadcastReceiver {
    private static EditText editText, editText_two, editText_three, editText_four;

    public void setEditText_two(EditText editText_two) {
        OTP_Receiver.editText_two = editText_two;
    }

    public void setEditText_three(EditText editText_three) {
        OTP_Receiver.editText_three = editText_three;
    }

    public void setEditText_four(EditText editText_four) {
        OTP_Receiver.editText_four = editText_four;
    }

    public void setEditText(EditText editText) {
        OTP_Receiver.editText = editText;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        SmsMessage[] messages = new SmsMessage[0];
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            messages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
        }

        for (SmsMessage sms : messages) {
            String message = sms.getMessageBody();
            String text = message.replaceAll("[^0-9]", "");
//            String otp;
//            try {
//                 otp = message.split("is ")[1];
//            }catch (Exception e){
//                otp = message.split(":")[1];
//            }
//            String number = otp;
            char one = text.charAt(0);
            char two = text.charAt(1);
            char three = text.charAt(2);
            char four = text.charAt(3);

            String first = String.valueOf(one);
            String second = String.valueOf(two);
            String third = String.valueOf(three);
            String fourth = String.valueOf(four);

            Log.e("otp", text);
            Log.e("otp1", String.valueOf(one));
            Log.e("otp1", String.valueOf(two));
            Log.e("otp1", String.valueOf(three));
            Log.e("otp1", String.valueOf(four));
            editText.setText(first);
            editText_two.setText(second);
            editText_three.setText(third);
            editText_four.setText(fourth);

        }
    }
}