package com.example.digitalcard.utils;

import android.content.Context;
import android.util.Log;
import android.widget.RelativeLayout;

import com.example.digitalcard.activity.HomePage;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.digitalcard.utils.ApiUrls.base_url;
import static com.example.digitalcard.utils.ApiUrls.main_domain;

/**
 * Created by Vinayak Ravi 20/04/2021
 */

public class NetworkClient {

    private static final String BASE_URL = base_url;
    private static Retrofit retrofit;
    private static String auth_Token = HomePage.user_id;


    public static Retrofit getRetrofitClient(Context context) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(GetClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static OkHttpClient GetClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(8, TimeUnit.MINUTES)
                .readTimeout(8, TimeUnit.MINUTES)
                .writeTimeout(8, TimeUnit.MINUTES);
        if (auth_Token != null && !auth_Token.equals("")) {
            okHttpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Log.e("token", auth_Token);
                    Request request = chain.request().newBuilder().addHeader("Authorization", "Bearer " + auth_Token).build();
                    return chain.proceed(request);
                }


            });
        }
        return okHttpClient.addInterceptor(logging).build();
    }
}
