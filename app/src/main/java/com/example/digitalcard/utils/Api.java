package com.example.digitalcard.utils;


import android.content.SharedPreferences;

import com.example.digitalcard.activity.HomePage;
import com.example.digitalcard.pojo.AddServicePojo;
import com.example.digitalcard.pojo.BusinessDetailPojo;
import com.example.digitalcard.pojo.CreateUserRequest;
import com.example.digitalcard.pojo.CreateUserResponse;
import com.google.gson.JsonObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Url;

import static com.example.digitalcard.utils.ApiUrls.createCard;
import static com.example.digitalcard.utils.ApiUrls.createUser;

/**
 * Created by Vinayak Ravi 20/04/2021
 */

public interface Api {

//    @POST(createUser + "{id}")
//    Call<MainObject> createUser(@Path("id") String id, @Body MainObject login);


    @POST("user")
    Call<ResponseBody> createUser(
            @Body CreateUserRequest mainObject);

//    @POST("user")
//    Call<CreateUserResponse> createUser(
//            @Body CreateUserRequest mainObject);

    @FormUrlEncoded
    @POST("business-card/create")
    Call<ResponseBody> createBusinessCard(
            @Field("businessName") String businessName);

    @PUT("business-card/update/{id}")
    Call<ResponseBody> aboutUs(
            @Path("id") String id,
            @Body BusinessDetailPojo businessDetailPojo);

    @GET()
    Call<ResponseBody> businessCardData(
            @Url String url
    );

    @GET()
    Call<ResponseBody> pinCode(
            @Url String url);

    @PUT()
    Call<ResponseBody> AddServices(
            @Url String url,
            @Body AddServicePojo addServicePojo);

    @PUT()
    Call<ResponseBody> AddProducts(
            @Url String url,
            @Body AddServicePojo addServicePojo);

    @PUT()
    Call<ResponseBody> updateProducts(
            @Url String url,
            @Body AddServicePojo addServicePojo);

    @PUT()
    Call<ResponseBody> updateService(
            @Url String url,
            @Body AddServicePojo addServicePojo);

    @FormUrlEncoded
    @PUT()
    Call<ResponseBody> socialLink(
            @Url String url,
            @Field("name") String name,
            @Field("link") String link,
            @Field("isVisible") Boolean isVisible);

    @FormUrlEncoded
    @PUT()
    Call<ResponseBody> updatesocialLink(
            @Url String url,
            @Field("name") String name,
            @Field("link") String link,
            @Field("isVisible") Boolean isVisible);


}
