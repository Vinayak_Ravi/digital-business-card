package com.example.digitalcard.utils;

/**
 * Created by Vinayak Ravi 20/04/2021
 */

public class ApiUrls {

    public static String main_domain = "http://52.66.199.155:4000/";
    public static final String base_url = main_domain + "api/v1/";

    public static final String createUser = base_url + "user";
    public static final String createCard = base_url + "business-card/create";
    public static final String about_Us = base_url + "business-card/update/";
    public static final String cardData = base_url + "business-card";
    public static final String addSocialLinks = base_url + "business-card/add-social-links/";
    public static final String updateSocialLinks = base_url + "business-card/update-social-links/";
    public static final String addServices = base_url + "business-card/add-service/";
    public static final String updateServices = base_url + "business-card/update-service/";
    public static final String addProducts = base_url + "business-card/add-product/";
    public static final String updateProducts = base_url + "business-card/update-product/";


}
